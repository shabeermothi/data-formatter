/**
 * Created by shabeermothi on 07/09/14.
 */

// DataFormatter for series charts
var SeriesChartsDataFormatter;

SeriesChartsDataFormatter = {
    responseData: "",
    getUniqueValues: function (value, index, self) {
        return self.indexOf(value) === index;
    },
    formatData: function () {

        var chartResponseObj = {
            categories: [],
            data: []
        };

        var globalMap = new Map(), dataMap = new Map(), dataArr = [], categoryArr = [];

        if (this.responseData != null) {
            this.responseData.forEach(function (val, key) {

                if (dataMap.get(val[Object.keys(val)[1]]) != null) {
                    globalMap.set(val[Object.keys(val)[0]],
                        val[Object.keys(val)[2]]);
                    dataMap.set(val[Object.keys(val)[1]],
                        globalMap);
                } else {
                    globalMap = new Map();
                    dataArr = [];
                    dataArr.push(val[Object.keys(val)[2]]);
                    globalMap.set(val[Object.keys(val)[0]],
                        val[Object.keys(val)[2]]);
                    dataMap.set(val[Object.keys(val)[1]],
                        globalMap);
                }

                categoryArr.push(val[Object.keys(val)[0]]);
            });
        } else {
            console.log("DataFormatter :: No data returned from server");
        }

        categoryArr = categoryArr.filter(this.getUniqueValues);

        dataMap.forEach(function (value, key) {
            var dataObj = {
                name: "",
                data: []
            };

            dataObj.name = key;

            for (category in categoryArr) {
                if (value.get(categoryArr[category]))
                    dataObj.data.push((typeof value.get(categoryArr[category]) == "string") ? parseInt(value.get(categoryArr[category])) : value.get(categoryArr[category]));
                else
                    dataObj.data.push(null);
            }

            chartResponseObj.data.push(dataObj);
        });

        chartResponseObj.categories.push(categoryArr);

        return chartResponseObj;
    }
};

// Data Formatter for scatterplot charts
// Expected source data format
/*  var sourceData = {
        "data": [
            {
                "name": "Female",
                "height": 10,
                "weight": 100
            },
            {
                "name": "Female",
                "height": 10,
                "weight": 200
            },
            {
                "name": "Male",
                "height": 8,
                "weight": 150
            },
            {
                "name": "Male",
                "height": 10,
                "weight": 190
            }

        ]
    };
*/
var ScatterPlotDataFormatter;

ScatterPlotDataFormatter = {
    responseData: "",
    getUniqueValues: function (value, index, self) {
        return self.indexOf(value) === index;
    },
    formatData: function () {

        var chartResponseObj, chartResponseArr = [] , categoryMap = new Map(), dataArr = [];

        this.responseData.forEach(function (value, key) {

            var dataElementArr = [];

            if(categoryMap.get(value[Object.keys(value)[0]]) != null){

                dataElementArr.push(value[Object.keys(value)[1]]);
                dataElementArr.push(value[Object.keys(value)[2]]);

                categoryMap.get(value[Object.keys(value)[0]]).push(dataElementArr);
            }else{
                dataArr = [];

                dataElementArr.push(value[Object.keys(value)[1]]);
                dataElementArr.push(value[Object.keys(value)[2]]);

                dataArr.push(dataElementArr);

                categoryMap.set(value[Object.keys(value)[0]],dataArr);
            }
        });

        categoryMap.forEach(function(value,key){

            chartResponseObj = {
                name: "",
                data: []
            };

            chartResponseObj.name = key;
            chartResponseObj.data = value;

            chartResponseArr.push(chartResponseObj);
        });

        return chartResponseArr;
    }
};

// Data Formatter for Heatmap charts
// Expected source data format
/* var dataPoints = {
  "data" : [
    {
      "activity" : "Marvell",
      "date" : "5-Sep",
      "time" : 10
    },
    {
      "activity" : "Marvell",
      "date" : "6-Sep",
      "time" : 18
    },
    {
      "activity" : "Tony Stark",
      "date" : "5-Sep",
      "time" : 8
    },
    {
      "activity" : "Tony Stark",
      "date" : "6-Sep",
      "time" : 16
    }
  ]
};
*/
var HeatmapDataFormatter;
HeatmapDataFormatter = {
	  responseData: "",
	  // Gets the unique value of provided data
	  getUniqueValues: function(value,index,self){
	    return self.indexOf(value) === index;
	  },
	  // Formats the input data
	  formatData: function(){
	    
		var chartReponseData = {
	      xCategories : [],
	      yCategories : [],
	      data : []
	    };
	    
	    var globalMap = new Map(), dataMap = new Map(), xCategoryArr = [], dataArr = [], yCategoryArr = [];
	    
	    // Iterate data to get the key and values from dict
	    this.responseData.forEach(function(value,key){
	        
	        // If key does not exist in the map 
	        // Create a new map instance of dataMap
	        if(!globalMap.get(value[Object.keys(value)[0]]))
	          dataMap = new Map();  
	          
	          // dataMap containing first element as key 
	          // and second element as value
	          dataMap.set(value[Object.keys(value)[1]], 
	                     value[Object.keys(value)[2]]);
	      
	          // globalMap having first element as key
	          // and dataMap as value
	          globalMap.set(value[Object.keys(value)[0]],dataMap); 
	      
	          // First and second elements pushed to array
	          xCategoryArr.push(value[Object.keys(value)[0]]);
	          yCategoryArr.push(value[Object.keys(value)[1]]);
	    });
	    
	    // Distinct Values of xCategoryArr and yCategoryArr 
	    // being pushed to chartResponseData
	    chartReponseData.xCategories = xCategoryArr.filter(this.getUniqueValues); 
	    chartReponseData.yCategories = yCategoryArr.filter(this.getUniqueValues); 
	    
	    // Iterate globalMap to push map elements
	    // into dataArr
	     globalMap.forEach(function(value,key){    
	         value.forEach(function(v,k){
	             var innerDataArr = [];
	             
	             innerDataArr.push(chartReponseData.xCategories.indexOf(key));
	             innerDataArr.push(chartReponseData.yCategories.indexOf(k));
	             innerDataArr.push(v);
	           
	             dataArr.push(innerDataArr);
	         }); 
	     }); 
	    
	    chartReponseData.data = dataArr; 
	     
	    return chartReponseData; 
	  }
}; 


// DataFomatting method which converts raw data to
// chart data with the help of formatters
var formatData;
formatData = function (objectToConvert, dataFormatter) {

    var responseObject;
    responseObject = Object.create(dataFormatter);
    responseObject.responseData = objectToConvert;

    return responseObject.formatData();
};
